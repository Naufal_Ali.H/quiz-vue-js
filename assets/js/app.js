var app = new Vue({
    el: '#app',
    data: {
        content:'',
        users:[
        {
            'name' : 'Muhammad Iqbal Mubarok',
        },
        {
            'name' : 'Ruby Purwanti',
        },
        {
            'name' : 'Faqih Muhammad',
        },
    ],
    },
    methods: {
        addUser: function () {
            this.users.push(this.content);
            this.content = '';
        },
        removeUser: function (index) {
            this.users.splice(index,1);
        },
        editUser: function (index) {
            this.users.edit(this.content);
            this.content = '';
        },
        updateUser: function (index) {
            this.users.update(this.content);
            this.content = '';
        }
    }
})